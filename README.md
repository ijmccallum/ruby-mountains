# Ruby mountain

A wee learning project to get to grips with Ruby on Rails.  
To avoid Ruby on Rails install pains, I'm running the dev environment in docker.

1. Build the container: `docker-compose build`
2. Run the container: `docker-compose run --rm --service-ports ruby_dev`
3. Run the server (cd into /rails-app): `rails s -p 3000 -b 0.0.0.0`

JS changes: `rails assets:precompile`
## THE PLAN! (What am I trying to make?!?)

I need help deciding which mountain / route to tackle next.  
To choose I need to filter/order routes by a few criteria:  
 - Distance from a set location .
 - OR roughly equilateral distance between 2 locations (for meeting friends who live farther afield).
 - Physicality, eg some formula of distance and slope. (Say the last hike was a little too much or too easy, find a hike that's a little less or a little more).
 - Technical difficulty (eg, my better half is not a fan of scrambling so I need to filter those out).
 - New peaks - so that I don't do the same ones over and over again!
 - Combined new peaks, if hiking with another user / someone who has a profile on one of the bagging sites.

Visualisations!

 - My bagged list.
 - Bagging comparison for those looking to compete!
 - Map view for prettieness / choosing routes.
   - potential next routes
   - show my bagged peaks
   - show hiking friends bagged peaks

## How I've set up the app so far

- **Summits** Static list of mountains to bag. User can check bagged to add it to their own bag list.  
  - Scaffold
  - data is seeded in. TODO: do we let people edit this?
- **Routes** Static list of routes you can take to bag 1 or more Summits.  
  - Scaffold `rails g scaffold route name:string description:text`
  - TODO: seed data in
- **Hikes** Like a diary entry, 1 or many users can go on a Hike. Hike can have 1 or more Summits. Created by a user, can add other users and Summits.  
  - Scaffold `rails g scaffold hike title:string description:text date:datetime user:references route:references` TODO: multiple users reference?
  - Hike is a relationship
- **Users** Your profile. List summits bagged by going through your list of bagged summits then going through your list of Hikes and looking for summits in them.  
  - Devise gem

### Relationships
 - **Bagged Summits**: Users <> Summits relation (many to many): `rails g scaffold bagged_summits datetime:date summit:references user:references` 
   - One User can bag multiple Summits. One Summit can be bagged by multiple Users.
   - Scaffold
 - Summits <> Hikes relation (many to many): `rails g model hikes_summits summit:references hike:references`
   - One Hike can hit multiple Summits
   - One Summit can be reached by multiple Hikes
 - Summits <> Routes relation (many to many): `rails g model routes_summits summit:references route:references`
   - One Route can hit multiple Summits
   - One Summit can be reached by multiple Routes
 - Users <> Routes relation (many to many): `rails g model route_users route:references user:references`
   - One User can bag multiple Routes
   - One Route can be bagged by multiple Users
 - Hike belongs to Route: code added to the create hikes migration
   - One Hike will only ever follow one Route
   - One Route can be Hiked multiple times

Run these when spinning up the app with a new db:
 - Set up the db: `rails db:migrate RAILS_ENV=development`
 - Summit seed data: `rails db:seed`

## Next up

 - Filtering!
   - Finish adding all the filters to the buttons & table (squish them in the table cols a bit)




## If going live:

 - update `config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }` in config/environments/production.rb





What's the hardest part to change / work on for you?
Gnarliest?

How free are the teams to choose their direciton?