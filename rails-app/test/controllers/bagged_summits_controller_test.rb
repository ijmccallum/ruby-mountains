require "test_helper"

class BaggedSummitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bagged_summit = bagged_summits(:one)
  end

  test "should get index" do
    get bagged_summits_url
    assert_response :success
  end

  test "should get new" do
    get new_bagged_summit_url
    assert_response :success
  end

  test "should create bagged_summit" do
    assert_difference('BaggedSummit.count') do
      post bagged_summits_url, params: { bagged_summit: { datetime: @bagged_summit.datetime, summit_id: @bagged_summit.summit_id, user_id: @bagged_summit.user_id } }
    end

    assert_redirected_to bagged_summit_url(BaggedSummit.last)
  end

  test "should show bagged_summit" do
    get bagged_summit_url(@bagged_summit)
    assert_response :success
  end

  test "should get edit" do
    get edit_bagged_summit_url(@bagged_summit)
    assert_response :success
  end

  test "should update bagged_summit" do
    patch bagged_summit_url(@bagged_summit), params: { bagged_summit: { datetime: @bagged_summit.datetime, summit_id: @bagged_summit.summit_id, user_id: @bagged_summit.user_id } }
    assert_redirected_to bagged_summit_url(@bagged_summit)
  end

  test "should destroy bagged_summit" do
    assert_difference('BaggedSummit.count', -1) do
      delete bagged_summit_url(@bagged_summit)
    end

    assert_redirected_to bagged_summits_url
  end
end
