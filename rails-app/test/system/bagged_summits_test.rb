require "application_system_test_case"

class BaggedSummitsTest < ApplicationSystemTestCase
  setup do
    @bagged_summit = bagged_summits(:one)
  end

  test "visiting the index" do
    visit bagged_summits_url
    assert_selector "h1", text: "Bagged Summits"
  end

  test "creating a Bagged summit" do
    visit bagged_summits_url
    click_on "New Bagged Summit"

    fill_in "Datetime", with: @bagged_summit.datetime
    fill_in "Summit", with: @bagged_summit.summit_id
    fill_in "User", with: @bagged_summit.user_id
    click_on "Create Bagged summit"

    assert_text "Bagged summit was successfully created"
    click_on "Back"
  end

  test "updating a Bagged summit" do
    visit bagged_summits_url
    click_on "Edit", match: :first

    fill_in "Datetime", with: @bagged_summit.datetime
    fill_in "Summit", with: @bagged_summit.summit_id
    fill_in "User", with: @bagged_summit.user_id
    click_on "Update Bagged summit"

    assert_text "Bagged summit was successfully updated"
    click_on "Back"
  end

  test "destroying a Bagged summit" do
    visit bagged_summits_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bagged summit was successfully destroyed"
  end
end
