require "application_system_test_case"

class SummitsTest < ApplicationSystemTestCase
  setup do
    @summit = summits(:one)
  end

  test "visiting the index" do
    visit summits_url
    assert_selector "h1", text: "Summits"
  end

  test "creating a Summit" do
    visit summits_url
    click_on "New Summit"

    fill_in "Description", with: @summit.description
    fill_in "Name", with: @summit.name
    click_on "Create Summit"

    assert_text "Summit was successfully created"
    click_on "Back"
  end

  test "updating a Summit" do
    visit summits_url
    click_on "Edit", match: :first

    fill_in "Description", with: @summit.description
    fill_in "Name", with: @summit.name
    click_on "Update Summit"

    assert_text "Summit was successfully updated"
    click_on "Back"
  end

  test "destroying a Summit" do
    visit summits_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Summit was successfully destroyed"
  end
end
