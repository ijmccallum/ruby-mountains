Rails.application.routes.draw do
  resources :bagged_summits
  resources :routes
  devise_for :users
  resources :hikes
  root 'home#index'
  get 'home/index', to: "home#index"
  # get 'summits', to: "summits#index"
  resources :summits
end
