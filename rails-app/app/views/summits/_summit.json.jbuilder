json.extract! summit, :id, :name, :description, :created_at, :updated_at
json.url summit_url(summit, format: :json)
