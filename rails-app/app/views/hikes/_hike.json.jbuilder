json.extract! hike, :id, :date, :description, :title, :created_at, :updated_at
json.url hike_url(hike, format: :json)
