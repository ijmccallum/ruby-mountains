json.extract! bagged_summit, :id, :datetime, :summit_id, :user_id, :created_at, :updated_at
json.url bagged_summit_url(bagged_summit, format: :json)
