const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

// const comparer = (idx, asc) => (a, b) => ((v1, v2) => {
//     reutv1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
//     )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
// }

const comparer = (idx, asc) => { 
  return function(a, b) { 
      return function(v1, v2) {
          console.log('beep')
          return (v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2)) 
              ? v1 - v2 
              : v1.toString().localeCompare(v2);
      }(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
  }
};

// do the work...
document.querySelectorAll('.sortable-th').forEach(th => th.addEventListener('click', (() => {
    const tbody = th.closest('table').querySelector('tbody');
    console.log('1');
    const arr = Array.from(tbody.querySelectorAll('tr:nth-child(n+2)'));
    console.log('2');
    arr.sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc));
    console.log('3');
    arr.forEach(tr => tbody.appendChild(tr) );
    console.log('4');
})));