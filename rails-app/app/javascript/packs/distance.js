
// My location manually set
const mylat = 55.949711;
const mylong = -3.098420;

const mLat = 56.453851;
const mLong = -3.992057;
const halfPie = Math.PI/180
//45ms

// const getDFast = (lat1, lon1, lat2, lon2) => {
//   var a = lat1 - lat2;
//   var b = lon1 - lon2;
//   return Math.sqrt( a*a + b*b ) * 76.7349282288;
// }

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 

  const halfDLatSin = Math.sin(dLat/2);
  const halfDLonSin = Math.sin(dLon/2);
  var a = 
    halfDLatSin * halfDLatSin +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    halfDLonSin * halfDLonSin; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km

  return d.toFixed(1);
}

function deg2rad(deg) {
  return deg * halfPie
}

var mountains = document.getElementsByClassName("js-distance");

for (var i = 0; i < mountains.length; i++) {
  mountains[i].innerHTML = getDistanceFromLatLonInKm(mylat, mylong, mountains[i].dataset.lat, mountains[i].dataset.lng);
}
  
