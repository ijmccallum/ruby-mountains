class BaggedSummitsController < ApplicationController
  before_action :set_bagged_summit, only: %i[ show edit update destroy ]

  before_action :authenticate_user!

  # GET /bagged_summits or /bagged_summits.json
  def index
    @bagged_summits = BaggedSummit.all
  end

  # GET /bagged_summits/1 or /bagged_summits/1.json
  def show
  end

  # GET /bagged_summits/new
  def new
    @bagged_summit = BaggedSummit.new
  end

  # GET /bagged_summits/1/edit
  def edit
  end

  # POST /bagged_summits or /bagged_summits.json
  def create
    @bagged_summit = BaggedSummit.new(bagged_summit_params)
    print @bagged_summit
    print '<<<<<><>>>>>>'
    @bagged_summit.user_id = current_user.id
    print @bagged_summit

    respond_to do |format|
      if @bagged_summit.save
        format.html { redirect_to @bagged_summit, notice: "Bagged summit was successfully created." }
        format.json { render :show, status: :created, location: @bagged_summit }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @bagged_summit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bagged_summits/1 or /bagged_summits/1.json
  def update
    respond_to do |format|
      if @bagged_summit.update(bagged_summit_params)
        format.html { redirect_to @bagged_summit, notice: "Bagged summit was successfully updated." }
        format.json { render :show, status: :ok, location: @bagged_summit }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @bagged_summit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bagged_summits/1 or /bagged_summits/1.json
  def destroy
    @bagged_summit.destroy
    respond_to do |format|
      format.html { redirect_to bagged_summits_url, notice: "Bagged summit was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bagged_summit
      @bagged_summit = BaggedSummit.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def bagged_summit_params
      params.require(:bagged_summit).permit(:datetime, :summit_id, :user_id)
    end
end
