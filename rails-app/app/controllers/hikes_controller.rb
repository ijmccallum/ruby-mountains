class HikesController < ApplicationController
  before_action :set_hike, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :is_participant, only: [:show, :edit, :update, :destroy]

  # GET /hikes or /hikes.json
  def index
    # TODO: only show hikes by the current_user
    # @hikes = Hike.find_by
  end

  # GET /hikes/1 or /hikes/1.json
  def show
  end

  # GET /hikes/new
  def new
    @hike = Hike.new
  end

  # GET /hikes/1/edit
  def edit
  end

  # POST /hikes or /hikes.json
  def create
    @hike = Hike.new(hike_params)

    # TODO: add current_user as hike author / user / relation / association

    respond_to do |format|
      if @hike.save
        format.html { redirect_to @hike, notice: "Hike was successfully created." }
        format.json { render :show, status: :created, location: @hike }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @hike.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hikes/1 or /hikes/1.json
  def update

    # TODO: user can only remove themselves from association, not others

    respond_to do |format|
      if @hike.update(hike_params)
        format.html { redirect_to @hike, notice: "Hike was successfully updated." }
        format.json { render :show, status: :ok, location: @hike }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @hike.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hikes/1 or /hikes/1.json
  def destroy

    # TODO: user can only destroy a hike if there are no other users associated with it.

    @hike.destroy
    respond_to do |format|
      format.html { redirect_to hikes_url, notice: "Hike was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  # TODO: action to check if the user makeing a request is one of the Hike participants (relationship / association)
  # def is_participant 
  #   @participant = 
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hike
      @hike = Hike.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def hike_params
      params.require(:hike).permit(:date, :description, :title)
    end
end
