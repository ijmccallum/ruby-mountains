class Hike < ApplicationRecord
  has_many :users, through: :hikes_user
  has_many :summits, through: :hikes_summit
  belongs_to :route
end
