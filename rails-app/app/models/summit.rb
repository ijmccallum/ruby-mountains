class Summit < ApplicationRecord
  scope :filterby, -> (urlQueryString) { 
    
    
    if urlQueryString
      dbFilterArray = []

      urlQueryString.split(",").each {
        |filter|
        case filter
        when 'munroe'
          dbFilterArray.push('summits.M','summits.MT','summits.xMT')
        when 'corbett'
          dbFilterArray.push('summits.C','summits.CT','summits.xC')
        when 'graham'
          dbFilterArray.push('summits.G','summits.GT','summits.xG')
        when 'marylin'
          dbFilterArray.push('summits.Ma','summits.sMa')
        when 'hump'
          dbFilterArray.push('summits.Hu','summits.sHu')
        when 'tump'
          dbFilterArray.push('summits.none_hundreder','summits.one_hundreder','summits.two_hundreder','summits.three_hundreder','summits.four_hundreder','summits.five_hundreder')
        when '0'
          dbFilterArray.push('summits.none_hundreder')
        when '1'
          dbFilterArray.push('summits.one_hundreder')
        when '2'
          dbFilterArray.push('summits.two_hundreder')
        when '3'
          dbFilterArray.push('summits.three_hundreder')
        when '4'
          dbFilterArray.push('summits.four_hundreder')
        when '5'
          dbFilterArray.push('summits.five_hundreder')
        when 'simm'
          dbFilterArray.push('summits.Sim','summits.sSim')
        when 'dodd'
          dbFilterArray.push('summits.five_hundreder','summits.s5','summits.four_hundreder')
        when 'furth'
          dbFilterArray.push('summits.F')
        when 'donald'
          dbFilterArray.push('summits.D','summits.DT')
        when 'dillon'
          dbFilterArray.push('summits.Dil')
        when 'highlandfive'
          dbFilterArray.push('summits.HF')
        when 'dewey'
          dbFilterArray.push('summits.DDew')
        when 'county'
          dbFilterArray.push('summits.CoU')
        when 'sib'
          dbFilterArray.push('summits.SIB')
        when 'wainwrights'
          dbFilterArray.push('summits.W','summits.WO')
        when 'hewitt'
          dbFilterArray.push('summits.Hew')
        when 'nuttal'
          dbFilterArray.push('summits.N')
        end
      }

      where(dbFilterArray.join(' OR '));
    end
    #if query has 'munroe' : where M OR MT
    # where('summits.M = ? || summits.C = ?', munroe, corbett) 
  }
  # scope :munroe, -> (query) { where('summits.M = ?', true) }
  # scope :corbett, -> (query) { where('summits.C = ?', true) }
  has_many :bagged_summits
  has_many :users, through: :bagged_summits
end
