class Route < ApplicationRecord
  has_many :users, through: :route_user
  has_many :summits, through: :route_summit
  has_many :hikes
end
