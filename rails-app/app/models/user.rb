class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_and_belongs_to_many :hikes
  has_many :hikes, through: :hike_user
  has_many :bagged_summits
  has_many :summits, through: :bagged_summits
  has_many :routes, through: :route_user
end
