class BaggedSummit < ApplicationRecord
  belongs_to :summit
  belongs_to :user
end
