# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_29_193238) do

  create_table "bagged_summits", force: :cascade do |t|
    t.date "datetime"
    t.integer "summit_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["summit_id"], name: "index_bagged_summits_on_summit_id"
    t.index ["user_id"], name: "index_bagged_summits_on_user_id"
  end

  create_table "hikes", force: :cascade do |t|
    t.datetime "date"
    t.text "description"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "route_id"
    t.index ["route_id"], name: "index_hikes_on_route_id"
  end

  create_table "hikes_summits", force: :cascade do |t|
    t.integer "summit_id", null: false
    t.integer "hike_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["hike_id"], name: "index_hikes_summits_on_hike_id"
    t.index ["summit_id"], name: "index_hikes_summits_on_summit_id"
  end

  create_table "route_users", force: :cascade do |t|
    t.integer "route_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["route_id"], name: "index_route_users_on_route_id"
    t.index ["user_id"], name: "index_route_users_on_user_id"
  end

  create_table "routes", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "routes_summits", force: :cascade do |t|
    t.integer "summit_id", null: false
    t.integer "route_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["route_id"], name: "index_routes_summits_on_route_id"
    t.index ["summit_id"], name: "index_routes_summits_on_summit_id"
  end

  create_table "summits", force: :cascade do |t|
    t.text "description"
    t.boolean "none_hundreder"
    t.boolean "one_hundreder"
    t.boolean "two_hundreder"
    t.boolean "three_hundreder"
    t.boolean "four_hundreder"
    t.boolean "five_hundreder"
    t.integer "number"
    t.string "name"
    t.string "parent_smc"
    t.string "parent_ma"
    t.integer "meters"
    t.integer "feet"
    t.integer "drop"
    t.integer "col_height"
    t.string "feature"
    t.string "survey"
    t.decimal "lat"
    t.decimal "lng"
    t.boolean "Ma"
    t.boolean "Hu"
    t.boolean "Tu"
    t.boolean "Sim"
    t.boolean "M"
    t.boolean "MT"
    t.boolean "F"
    t.boolean "C"
    t.boolean "G"
    t.boolean "D"
    t.boolean "DT"
    t.boolean "Hew"
    t.boolean "N"
    t.boolean "Dew"
    t.boolean "DDew"
    t.boolean "HF"
    t.boolean "W"
    t.boolean "WO"
    t.boolean "B"
    t.boolean "Sy"
    t.boolean "Fel"
    t.boolean "CoH"
    t.boolean "CoU"
    t.boolean "CoA"
    t.boolean "CoL"
    t.boolean "SIB"
    t.boolean "sMa"
    t.boolean "sHu"
    t.boolean "sSim"
    t.boolean "s5"
    t.boolean "s4"
    t.boolean "Mur"
    t.boolean "CT"
    t.boolean "GT"
    t.boolean "BL"
    t.boolean "Bg"
    t.boolean "Y"
    t.boolean "Cm"
    t.boolean "T100"
    t.boolean "xMT"
    t.boolean "xC"
    t.boolean "xG"
    t.boolean "xN"
    t.boolean "xDT"
    t.boolean "Dil"
    t.boolean "VL"
    t.boolean "A"
    t.boolean "Ca"
    t.boolean "Bin"
    t.boolean "O"
    t.boolean "Un"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "bagged_summits", "summits"
  add_foreign_key "bagged_summits", "users"
  add_foreign_key "hikes_summits", "hikes"
  add_foreign_key "hikes_summits", "summits"
  add_foreign_key "route_users", "routes"
  add_foreign_key "route_users", "users"
  add_foreign_key "routes_summits", "routes"
  add_foreign_key "routes_summits", "summits"
end
