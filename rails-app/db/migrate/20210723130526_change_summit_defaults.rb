class ChangeSummitDefaults < ActiveRecord::Migration[6.1]
  def change
    change_column(:summits, :created_at, :datetimem, :null => true)
    change_column(:summits, :updated_at, :datetimem, :null => true)
  end
end