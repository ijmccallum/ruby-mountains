class CreateRoutesSummits < ActiveRecord::Migration[6.1]
  def change
    create_table :routes_summits do |t|
      t.references :summit, null: false, foreign_key: true
      t.references :route, null: false, foreign_key: true

      t.timestamps
    end
  end
end
