class CreateSummits < ActiveRecord::Migration[6.1]
  def change
    create_table :summits do |t|
      t.text :description
      t.boolean :none_hundreder
      t.boolean :one_hundreder
      t.boolean :two_hundreder
      t.boolean :three_hundreder
      t.boolean :four_hundreder
      t.boolean :five_hundreder
      t.integer :number
      t.string :name
      t.string :parent_smc
      t.string :parent_ma
      t.integer :meters
      t.integer :feet
      t.integer :drop
      t.integer :col_height
      t.string :feature
      t.string :survey
      t.numeric :lat
      t.numeric :lng
      t.boolean :Ma
      t.boolean :Hu
      t.boolean :Tu
      t.boolean :Sim
      t.boolean :M
      t.boolean :MT
      t.boolean :F
      t.boolean :C
      t.boolean :G
      t.boolean :D
      t.boolean :DT
      t.boolean :Hew
      t.boolean :N
      t.boolean :Dew
      t.boolean :DDew
      t.boolean :HF
      t.boolean :W
      t.boolean :WO
      t.boolean :B
      t.boolean :Sy
      t.boolean :Fel
      t.boolean :CoH
      t.boolean :CoU
      t.boolean :CoA
      t.boolean :CoL
      t.boolean :SIB
      t.boolean :sMa
      t.boolean :sHu
      t.boolean :sSim
      t.boolean :s5
      t.boolean :s4
      t.boolean :Mur
      t.boolean :CT
      t.boolean :GT
      t.boolean :BL
      t.boolean :Bg
      t.boolean :Y
      t.boolean :Cm
      t.boolean :T100
      t.boolean :xMT
      t.boolean :xC
      t.boolean :xG
      t.boolean :xN
      t.boolean :xDT
      t.boolean :Dil
      t.boolean :VL
      t.boolean :A
      t.boolean :Ca
      t.boolean :Bin
      t.boolean :O
      t.boolean :Un

      t.timestamps
    end
  end
end
