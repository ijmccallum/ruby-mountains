class CreateHikes < ActiveRecord::Migration[6.1]
  def change
    create_table :hikes do |t|
      t.datetime :date
      t.text :description
      t.string :title
      t.timestamps
      t.belongs_to :route
    end
  end
end
