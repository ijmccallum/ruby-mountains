class CreateHikesSummits < ActiveRecord::Migration[6.1]
  def change
    create_table :hikes_summits do |t|
      t.references :summit, null: false, foreign_key: true
      t.references :hike, null: false, foreign_key: true

      t.timestamps
    end
  end
end
