class CreateBaggedSummits < ActiveRecord::Migration[6.1]
  def change
    create_table :bagged_summits do |t|
      t.date :datetime
      t.references :summit, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
