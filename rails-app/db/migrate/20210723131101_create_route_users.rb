class CreateRouteUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :route_users do |t|
      t.references :route, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
