FROM ruby:3.0.2

RUN apt-get update && apt-get install -y \
  build-essential \
  nodejs \
  npm

RUN ruby --version
RUN node --version
RUN npm --version
RUN npm i -g yarn
# RUN sqlite3 --version

RUN yarn --version

RUN gem install rails
RUN rails --version

RUN mkdir -p /app
WORKDIR /app

# If 
# rails new rails-app 

EXPOSE 3000

ENTRYPOINT [ "/bin/bash" ]

# CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]